A* Pathfinding
==========

The A* Pathfinding algorithm is an implementation of the A* (A-star) search algorithm written in 6510 assembly (8-bit version for Commodore 64/128).
It was developed for Steel Duck ([www.commocore.com/steel-duck](http://www.commocore.com/steel-duck)), but is fully configurable and can be easily reused.
It was initially written for 4-directional crawling, but support for diagonal directions can be easily implemented.
The algorithm was designed to work with a square map containing a maximum of 256 fields, and it can work on both open and labirynth-like areas.
This algorithm also allows work with different move costs per square, but a move cost equal to 1 is used in this implementation.
64tass cross-assembler ver. 1.51.675 has been used to compile the source file.


Modes
=====

The programme can work in manual run mode, or automatically. The compiled demo version in this repository works in full manual run mode.

You can set the mode by changing `PATHFINDING_MANUAL_RUN` variable in `debugging/demo.asm` file.

When `PATHFINDING_MANUAL_RUN = 1`, the programme will run in manual mode. The map, all marks, and the shortest path will be drawn on the screen in green.

To advance to the next step in manual mode, press fire (joystick port 2) or RETURN key.

When `PATHFINDING_DEBUG_MODE = 1`, additional test code is also included to check for any pitfalls, e.g.: 

	.byte 2 ; JAM!
	.text "debug#1"
	
If the programme crashes when reaching a test point, you'll see a string (e.g. "debug#1") in assembly monitor. This way you can easily find the exact place in the source code where the programme crashed.

When `PATHFINDING_DEBUG_TABLE_POINTER = 1`, some tables in `src/tables.asm` will be set with a PC counter for debugging purposes.


Demo usage
=========

Press fire button (joystick port 2) or ENTER to advance. Press F1 / F3 to switch map.


Project structure
============

The algorithm is separate from the demo, and can be easily implemented in other projects. You will find it in the `src` folder.
The `debugging` folder contains a demo for a step-by-step presentation of the algorithm, and a speed test (check the `debugging/build` folder for executables).
All unit tests are located in the `tests` folder. Unit tests use _c64unit_, which is imported in `vendor` folder.
Maps used for demos and unit tests are located in the `data/maps.asm` file.


Algorithm usage
============

The shortest path algorithm can be used to:
- find the shortest path to an exit
- find the shortest path to reach (trace) a moving object

When finding the shortest path to an exit (the default use case), the end point is assumed to be accessible.
To track a moving object, update the map by setting `0` (void) for positions where movable objects are currently located.
In any other case, call `isNodeAccessible` method to check if the end point is accessible (pointers for `pathfinding_mapPointer` needsto be set beforehand).


Before calling `findPath` method to run the algorithm, you need to set constants and variables.

Setting constants.
The constants are listed in the `src/constants.asm` file, but can be overwritten in your implementation:
	- `PATHFINDING_DEFAULT_VARIABLES_ADDRESSING = 1` - if not default, use your own addressing for all variables
	- `PATHFINDING_DEBUG_MODE = 1` - additional checkpoints for testing pitfall places
	- `PATHFINDING_DEBUG_TABLE_POINTER = 0` - if set, some tables in the includes/pathfinding/tables.asm file will be set with a PC counter for debugging purposes
	- `PATHFINDING_MANUAL_RUN = 1` - run programme in the manual mode, which draws the path, shows messages, and needs the user's interaction
	- `PATHFINDING_MAX_X = 13` - map rows
	- `PATHFINDING_MAX_Y = 8` - map columns

Setting variables.
The following variables need to be set before each call:
	- pointers `pathfinding_mapPointer+1` (lo) and `pathfinding_mapPointer+2` (hi) set the address of the map
	- pointers `pathfinding_trackPointer+1` (lo) and `pathfinding_trackPointer+2` (hi) set output track table position
	- start point variables: `pathfinding_xStart` and `pathfinding_yStart`
	- end point variables: `pathfinding_xEnd` and `pathfinding_yEnd`
	  If end point is reached, `endNodeReached` flag will be set to `1`. Otherwise it will be `0`.

To move variables on Zero Page to another location (all definitions are listed in `src/variables.asm` file), set `PATHFINDING_DEFAULT_VARIABLES_ADDRESSING = 0` and define the whole list in your implementation.

The algorithm will build a track under `pathfinding_trackPointer+1` (lo) and `pathfinding_trackPointer+2` (hi) address as a table with a sequence of node ids which defines the track.

Because the output track table position needs to be specified (maximum size of table: 256 bytes), it is possible to run the algorithm for more than one case, and keep output tracks as long as needed.
The algorithm is flexible enough to find and keep multiple tracks. You can always keep the same address location, and simply copy the result to a different memory location before using the algorithm again, but it is slower. 
The only variables to be preserved are`shortestPathLength` and `endNodeReached`, as both of them will be overwritten with the next execution. 
The algorithm can be called again only after the execution of the current one is finished.

Working example

A working example can be found in the `drawShortestPath` procedure in `debugging/includes/draw-shortest-path.asm` file. It marks the shortest path on the screen by reading from the created output track table.


Installing the algorithm package in your project
================================================

The easiest way to install the package is to use the installation script.
Copy `install/a-star-pathfinding-dependency.bat` (Windows), or `install/a-star-pathfinding-dependency.sh` (Linux,
Mac) script file from this repository to your main project folder. Execution of the script file creates
`vendor/a-star-pathfinding` folder, and clones the repository using _git_ version control system. 
The same script can be used to update the package version to the newest one.

Next, add `vendor` directory to the `.gitignore` file so it won't be included in your source code:

	vendor/*

To include the algorithm in your source code, just:

	.include "vendor/a-star-pathfinding/src/main.asm"
	
	
Speed test
==========

The speed of the algorithm can be tested by measuring computing time. Speed test demo (`debugging/speedtest.asm` source file) executes the algorithm on all maps, and displays results in a table on screen.
The measurement starts in IRQ when finding shortest path is triggered, and is presented in frames and rasterlines left.
Note that 1 frame contains 312 rasterlines in PAL system (63 cycles per rasterline), and the common NTSC system has 263 rasterlines (65 cycles on each rasterline).
An example result in PAL system could be: `2 frames and 255+7 rasterlines`, which means it took 2 * 312 + 255 + 7 = 886 rasterlines (55818 cycles) to execute the whole algorithm for one map.


Configuration
==========

- `PATHFINDING_MAX_X` - map width
- `PATHFINDING_MAX_Y` - map height


Unit tests
=======

_c64unit_ unit test framework has been used for unit tests.
To recompile and test the algorithm locally:
(1) execute `c64unit-dependency.bat` (Windows) or `c64unit-dependency.sh` (Linux, Mac) to install _c64unit_
(2) recompile the test suite and run to see assertion results.
You can find more information about _c64unit_ on the
[https://bitbucket.org/Commocore/c64unit](https://bitbucket.org/Commocore/c64unit)] repository page.

- The `tests` folder contains unit test cases with assertions.
It also contains a working example with different map setups which is asserted against result data for the shortest
path and calculated length. All examples use the Data Set feature from c64unit framework which allows to execute test with different scenarios.

When running tests:
- green screen border indicates that all tests passed
- red screen border means that the current test failed
You will see a detailed message about the assertion(s) which failed, with both expected and actual values provided.


Map
===

In `data/maps.asm` you'll find examples of map sets used in the working example, where: 
- 0 - void
- 1 - path


Changelog
========

### 2017-04-30
### Added
- public method `isNodeAccessible` to check if any given node is a path or void

### Changed
- Always treat the end point as accessible

### 2017-03-01
### Added
- DocBlock for all functions

### Changed
- Flexible output track pointers instead of hardcoded single table address

### 2017-02-26
### Changed
- Squeeze registers used on Zero Page
- Move variables to separate file
- Namespace for all constants and few common variable names

### 2017-02-23
### Changed
- Project folders structure
- Build subfolders for executables

### 2017-02-22
### Fixed
- Queue reorder

### Added
- Speed test

### 2017-02-21
### Fixed
- End node check bug
- Default to keyboard output when exiting from test to Basic

### Added
- In tests, set cursor to the row below progress indicator results

### 2017-02-13
### Changed
- Tests for pathfinding
- Init procedures in unit tests
- Change `DRAW` mode naming to `MANUAL_RUN`

### Fixed
- Assertion summary display in unit tests

### 2017-02-12
### Fixed
- Drawing output track by adding shortest path length register

### 2017-02-09
### Added
- Ability to switch map to another with F1/F3 keys

### 2017-02-08
### Added
- Building output track with shortest path
- Showing shortest path

### Changed
- Clean up for methods and variables

### 2017-02-02
### Added
- Recording parent nodes
- Displaying node ids in hex

### 2017-02-01
### Added
- Test suite implementation for c64unit test framework
- Test for sorting queue list by score

### Changed
- c64unit test framework refactor, and detach from test cases by moving to includes directory

### 2017-01-31
### Added
- Queueing functionality to sort nodes by lowest total score values

### Fixed
- Heuristic values table calculation

### 2017-01-30
### Added
- Main "flooding" and list queueing
- Heuristic values table calculation

### 2017-01-29
### Added
- Extract main functionality from experimental pathfinding project to work on A* pathfinding implementation
