
	; Include c64unit
	.include "../vendor/c64unit/cross-assemblers/64tass/core2000.asm"

	; Constants used across whole testsuite
	PATHFINDING_MAX_X = 13
	PATHFINDING_MAX_Y = 8
	PATHFINDING_DEBUG_TABLE_POINTER = 0
	PATHFINDING_DEBUG_MODE = 1
	PATHFINDING_MANUAL_RUN = 0
	PATHFINDING_DEFAULT_VARIABLES_ADDRESSING = 1
	MAX_MAPS = 21 ; how many maps to test
	OUTPUT_TRACK_ADDRESS = $8000
	
	; c64unit settings
	C64UNIT_EXIT_TO_BASIC = 1

	; Handling test variables
	testPositionPointer = $70 ;word


	; Init c64unit
	c64unit
	
	; Examine test cases
	examineTest sortQueueListByScoreTest
	examineTest findPathTest
	
	; If this point is reached, there were no assertion fails
	c64unitExit

	; Include domain logic, i.e. classes, methods and tables
	.include "../src/main.asm"
	
	; Test cases
	.include "test-cases/sort-queue-list-by-score/test.asm"
	.include "test-cases/pathfinding/test.asm"
	