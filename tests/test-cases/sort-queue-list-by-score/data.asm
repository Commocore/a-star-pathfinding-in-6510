
; Data to be set
queueData .byte 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ; node ids	

totalScoreData .byte 6, 5, 2, 7, 8, 4, 9, 10, 1, 3 ; total score values for corresponding node ids

; Data to be expected
queueListExpected .byte 8, 2, 9, 5, 1, 0, 3, 4, 6, 7 ; queue list order expected after sort
