
; @access public
; @return void	
sortQueueListByScoreTest .proc
	; Set up data for queueData and totalScoreData tables
	; In other words, let's simulate that path has been found, and we have 10 node elements in a queue
	; Data Set will be used here to set all data
	prepareDataSetLength 10
-
	getDataSet queueData
	ldy c64unit_dataSetIterator
	sta queueListTable,y
	
	getDataSet totalScoreData
	ldy c64unit_dataSetIterator
	sta totalScoreTable,y

	isDataSetCompleted
	bne -
	
	; Init variables for function
	lda #0
	sta previousTotalScore
	sta sortOrderAffected
	sta queueCursor
	
	lda #size(queueData)
	sta queueLength
	
	; Run function
	jsr sortQueueListByScore

	prepareDataSetLength 10
-
	getDataSet queueListTable
	assertDataSetEqualToA queueListExpected, "queue sort failed: order not equal."

	isDataSetCompleted
	bne -	
rts


; Include data
.include "data.asm"

.pend
