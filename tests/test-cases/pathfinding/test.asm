
; @access public
; @return void
findPathTest .proc
	
	prepareDataSetLength MAX_MAPS+1
-
	; Set map pointers for this particular map
	getDataSet mapAddressLocationLo
	sta pathfinding_mapPointer+1
	
	getDataSet mapAddressLocationHi
	sta pathfinding_mapPointer+2

	; Set start and end points for this particular map
	clc
	lda pathfinding_mapPointer+1
	adc #PATHFINDING_MAX_X * PATHFINDING_MAX_Y
	sta testPositionPointer
	lda pathfinding_mapPointer+2
	adc #0
	sta testPositionPointer+1

	ldy #0
	lda (testPositionPointer),y
	sta pathfinding_xStart

	iny
	lda (testPositionPointer),y
	sta pathfinding_yStart
	
	iny
	lda (testPositionPointer),y
	sta pathfinding_xEnd

	iny
	lda (testPositionPointer),y
	sta pathfinding_yEnd
	
	lda #<OUTPUT_TRACK_ADDRESS
	sta pathfinding_trackPointer+1
	lda #>OUTPUT_TRACK_ADDRESS
	sta pathfinding_trackPointer+2

	lda #0
	sta shortestPathLength
	
	jsr findPath
	
	; Assertions against the current map
	assertDataSetEqual endNodeReachedExpected, endNodeReached, "end point reached not equal."
	assertDataSetEqual lengthExpected, shortestPathLength, "shortest path length not equal."
	
	ldx pathfinding_xEnd
	ldy pathfinding_yEnd
	jsr isNodeAccessible
	assertDataSetEqualToA endNodeAccessibleExpected, "end node accessible not equal."

	isDataSetCompleted
	beq +
		jmp -
+

rts


; Include data
.include "data.asm"
.include "../../../data/maps.asm"

.pend
