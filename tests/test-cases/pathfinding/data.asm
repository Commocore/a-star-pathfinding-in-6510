
; Expected values
endNodeReachedExpected
	.byte 1 ; map0
	.byte 1 ; map1
	.byte 1 ; map2
	.byte 1 ; map3
	.byte 1 ; map4
	.byte 1 ; map5
	.byte 1 ; map6
	.byte 1 ; map7
	.byte 1 ; map8
	.byte 1 ; map9
	.byte 1 ; map10
	.byte 1 ; map11
	.byte 0 ; map12
	.byte 1 ; map13
	.byte 1 ; map14
	.byte 1 ; map15
	.byte 1 ; map16
	.byte 1 ; map17
	.byte 1 ; map18
	.byte 0 ; map19
	.byte 0 ; map20
	.byte 1 ; map21

lengthExpected
	.byte 19 ; map0
	.byte 19 ; map1
	.byte 21 ; map2
	.byte 15 ; map3
	.byte 14 ; map4
	.byte 13 ; map5
	.byte 13 ; map6
	.byte 13 ; map7
	.byte 13 ; map8
	.byte 9 ; map9
	.byte 15 ; map10
	.byte 15 ; map11
	.byte 0 ; map12
	.byte 17 ; map13
	.byte 13 ; map14
	.byte 17 ; map15
	.byte 10 ; map16
	.byte 24 ; map17
	.byte 17 ; map18
	.byte 0 ; map19
	.byte 0 ; map20
	.byte 8 ; map21

endNodeAccessibleExpected
	.byte 1 ; map0
	.byte 1 ; map1
	.byte 1 ; map2
	.byte 1 ; map3
	.byte 1 ; map4
	.byte 1 ; map5
	.byte 1 ; map6
	.byte 1 ; map7
	.byte 1 ; map8
	.byte 1 ; map9
	.byte 1 ; map10
	.byte 1 ; map11
	.byte 1 ; map12
	.byte 1 ; map13
	.byte 1 ; map14
	.byte 1 ; map15
	.byte 1 ; map16
	.byte 1 ; map17
	.byte 1 ; map18
	.byte 1 ; map19
	.byte 1 ; map20
	.byte 0 ; map21
