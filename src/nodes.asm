
; Returns node type, but will always return path type if it's the end point
;
; @access private
; @param X - position x of node
; @param Y - position y of node
; @return A - node value [0: void, 1: path (also node id will be saved in currentNodeId)]
getNodeType
	txa
	clc
	adc mapVerticalPosition,y
	sta currentNodeId
	cmp endNodeId
	bne +
		; If the node is a destination point, don't look for its type, but assume it's accessible
		; Useful when destination point is a moving object which is being followed
		lda #1
		rts
+
	tay
pathfinding_mapPointer
	lda $1234,y
rts


; @access private
; @param X - position x of node
; @param Y - position y of node
; @return A - node id
getNodeId
	txa
	clc
	adc mapVerticalPosition,y
rts


; Checks if node is accessible, no matter it's the end point or not (look: getNodeType)
;
; @access public
; @param X - position x of node
; @param Y - position y of node
; @return A - node value [0: void, 1: path]
isNodeAccessible
	lda pathfinding_mapPointer+1
	sta pathfinding_mapPointerNodeAccessible+1
	lda pathfinding_mapPointer+2
	sta pathfinding_mapPointerNodeAccessible+2
	txa
	clc
	adc mapVerticalPosition,y
	tay
pathfinding_mapPointerNodeAccessible
	lda $1234,y
rts
