
mapVerticalPosition
	.for y=0, y<PATHFINDING_MAX_Y, y=y+1
		.byte y*PATHFINDING_MAX_X
	.next

.cwarn * > nodesXTable, "FIXME: Tables overlapping!"
	
.if PATHFINDING_DEBUG_TABLE_POINTER
	* = $3800
.fi
; Table used to get x position from node id
nodesXTable
	.for y=0, y<PATHFINDING_MAX_Y, y=y+1
		.for x=0, x<PATHFINDING_MAX_X, x=x+1
			.byte x
		.next
	.next

.cwarn * > nodesYTable, "FIXME: Tables overlapping!"
	
.if PATHFINDING_DEBUG_TABLE_POINTER
	* = $3900
.fi
; Table used to get y position from node id
nodesYTable
	.for y=0, y<PATHFINDING_MAX_Y, y=y+1
		.for x=0, x<PATHFINDING_MAX_X, x=x+1
			.byte y
		.next
	.next

.cwarn * > queueListTable, "FIXME: Tables overlapping!"

.if PATHFINDING_DEBUG_TABLE_POINTER
	* = $4000
.fi
queueListTable
	.for i=0, i<PATHFINDING_MAX_X*PATHFINDING_MAX_Y, i=i+1
		.byte 0
	.next

.cwarn * > nodesListIndexTable, "FIXME: Tables overlapping!"

.if PATHFINDING_DEBUG_TABLE_POINTER
	* = $4100
.fi
nodesListIndexTable
	.for i=0, i<PATHFINDING_MAX_X*PATHFINDING_MAX_Y, i=i+1
		.byte 0
	.next

.cwarn * > heuristicTable, "FIXME: Tables overlapping!"

.if PATHFINDING_DEBUG_TABLE_POINTER
	* = $4200
.fi
heuristicTable
	.for i=0, i<PATHFINDING_MAX_X*PATHFINDING_MAX_Y, i=i+1
		.byte 0
	.next

.cwarn * > moveCostTable, "FIXME: Tables overlapping!"
	
.if PATHFINDING_DEBUG_TABLE_POINTER
	* = $4300
.fi
moveCostTable
	.for i=0, i<PATHFINDING_MAX_X*PATHFINDING_MAX_Y, i=i+1
		.byte 0
	.next

.cwarn * > totalScoreTable, "FIXME: Tables overlapping!"

.if PATHFINDING_DEBUG_TABLE_POINTER
	* = $4400
.fi
totalScoreTable
	.for i=0, i<PATHFINDING_MAX_X*PATHFINDING_MAX_Y, i=i+1
		.byte 0
	.next

.cwarn * > parentNodesTable, "FIXME: Tables overlapping!"

.if PATHFINDING_DEBUG_TABLE_POINTER
	* = $4500
.fi
parentNodesTable
	.for i=0, i<PATHFINDING_MAX_X*PATHFINDING_MAX_Y, i=i+1
		.byte 0
	.next


