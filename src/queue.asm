
; @access private
; @var - currentNodeId [needs to be set beforehand]
; @return void
addNodeToQueue

	ldx currentNodeId

	lda nodesListIndexTable,x
	beq +
		rts
+

	inc nodesListIndexTable,x ; acknowledge that this node has been analyzed already

	.if PATHFINDING_MANUAL_RUN
		ldy currentNodeId
		lda nodesXTable,y
		tax
		lda nodesYTable,y
		tay
		lda #2
		jsr drawSquare
	.fi

	lda queueLength
	tay

	lda currentNodeId
	sta queueListTable,y
	inc queueLength
	
	; Set move cost for this node
	tay ; transfer currentNodeId to y register
	lda currentMoveCost
	sta moveCostTable,y

	; Calculate total score (F = H + G)
	lda heuristicTable,y
	clc
	adc moveCostTable,y
	sta totalScoreTable,y

	; Set parent node
	lda parentNodeId
	ldy currentNodeId
	sta parentNodesTable,y

	; Check if end node has been reached
	lda currentNodeId
	cmp endNodeId
	bne +
		; End node found!
		inc endNodeReached
		jmp buildTrack
+

rts


; @access private
; @param A - node id
; @param Y - current position of this node on the opened list
; @return void
removeNodeFromQueue
	sta currentNodeId
	sty currentNodePositionOnList
	
	dec queueLength
	dec queueCursor
	
	.if PATHFINDING_MANUAL_RUN
		ldy currentNodeId
		lda nodesXTable,y
		tax
		lda nodesYTable,y
		tay
		lda #4
		jsr drawSquare
	.fi

	; Now, let's reorder opened list to eliminate null node values on opened list
	
	ldy currentNodePositionOnList
	
; @access private
; @param Y - position of node, from which reorder should begin to the last element of queue
reorderQueueList
	tya
	tax
	inx
	
	lda queueLength
	sta reorderQueueValue+1
	
-
	lda queueListTable,x
	sta queueListTable,y

	inx
	iny
reorderQueueValue
	cpy #0
	bcc -
rts


; @access private
; @throws jam
; @return void
sortQueueListByScore
	lda queueLength
	bne +
		rts
+

-
	ldx #0
	stx previousTotalScore
	stx sortOrderAffected
-
	lda queueListTable,x ; get node id
	tay
	lda totalScoreTable,y

	.if PATHFINDING_DEBUG_MODE
		bne +
			.byte 2 ; JAM!
			.text "debug#1" ; small trick to see this string in monitor, so it's easier to find this place in code
+
	.fi

	cmp previousTotalScore
	sta previousTotalScore
	bcc +
		inx
		cpx queueLength
		bne -
	
		; Repeat sorting until order has been changed
		lda sortOrderAffected
		bne --
		
		; If lowest score has been found on the list, reset queue cursor
		lda #$ff
		sta queueCursor
		
	rts
	
+
	; Total score is lower than previous one, so re-order both nodes with places
	inc sortOrderAffected ; acknowledge sort order flag
	
	lda queueListTable,x
	sta pathfinding_temp
	
	lda queueListTable-1,x
	sta queueListTable,x
	lda pathfinding_temp
	sta queueListTable-1,x
	jmp -
