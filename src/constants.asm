
; These constants can be overwritten
.weak
	PATHFINDING_DEFAULT_VARIABLES_ADDRESSING = 1 ; if not default, use your own addressing for all variables
	PATHFINDING_DEBUG_MODE = 1 ; additional checkpoints for testing pitfall places
	PATHFINDING_DEBUG_TABLE_POINTER = 0 ; if set, some tables in includes/pathfinding/tables.asm file will be set with PC counter for debug purposes
	PATHFINDING_MANUAL_RUN = 1 ; run program in manual mode, which draws the path, shows messages, needs user's interaction
	
	PATHFINDING_MAX_X = 13 ; map rows
	PATHFINDING_MAX_Y = 8 ; map columns
.endweak
