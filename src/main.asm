
; Public pathfinding function
;
; @access public
; @var pathfinding_mapPointer+1 - lo address map pointer [needs to be set beforehand]
; @var pathfinding_mapPointer+2 - hi address map pointer [needs to be set beforehand]
; @var pathfinding_trackPointer+1 - lo address output track pointer [needs to be set beforehand]
; @var pathfinding_trackPointer+2 - hi address output track pointer [needs to be set beforehand]
; @var pathfinding_xStart - start point x [needs to be set beforehand]
; @var pathfinding_yStart - start point y [needs to be set beforehand]
; @var pathfinding_xEnd - end point x [needs to be set beforehand]
; @var pathfinding_yEnd - end point y [needs to be set beforehand]
; @return void
findPath

	jsr cleanPathTables

	.if PATHFINDING_MANUAL_RUN
		jsr waitingStep
	.fi
	
	jsr calculateHeuristics
	
	; Reset variables
	lda #0
	sta queueCursor
	sta queueLength
	sta currentMoveCost
	sta endNodeReached
	sta startNodeId
	sta endNodeId

	ldx pathfinding_xEnd
	ldy pathfinding_yEnd
	jsr getNodeId
	sta endNodeId
	
	ldx pathfinding_xStart
	ldy pathfinding_yStart
	jsr getNodeId
	sta currentNodeId
	sta startNodeId
	jsr addNodeToQueue

	ldy #0
findPathLoop
	sty queueCursor

	; Get node from opened list and set x & y positions
	lda queueListTable,y ; get node id

	tay ; transfer node id to y register
	lda nodesXTable,y ; get node x
	sta currentX
	lda nodesYTable,y ; get node y
	sta currentY
	
	lda moveCostTable,y ; get node move cost
	adc #1 ; move cost node to neighbour square is always 1, but another types of terrain (move costs) can be implemented here
	sta currentMoveCost

	.if PATHFINDING_MANUAL_RUN
		jsr displayCurrentStatusMessage
		ldy queueCursor
		lda queueListTable,y
		tay
		jsr displayCurrentNodeMessage
		jsr waitingStep
		ldy queueCursor
		lda queueListTable,y
		tay
		
		; If map has been switched, exit from current path finding
		lda switchMapProcess
		beq +
			rts
+
	.fi

	; Move node to closed list
	tya ; transfer node id to accumulator
	sta parentNodeId
	ldy queueCursor
	jsr removeNodeFromQueue

	; Find node's neighbours for all 4 directions if available
	
	; Check north direction
	ldy currentY
	beq +
		dey
		ldx currentX
		jsr getNodeType
		beq +
			jsr addNodeToQueue
+
	
	; Check east direction
	ldx currentX
	cpx #PATHFINDING_MAX_X-1
	beq +
		inx
		ldy currentY
		jsr getNodeType
		beq +
			jsr addNodeToQueue
+

	; Check south direction
	ldy currentY
	cpy #PATHFINDING_MAX_Y-1
	beq +
		iny
		ldx currentX
		jsr getNodeType
		beq +
			jsr addNodeToQueue
+

	; Check west direction
	ldx currentX
	beq +
		dex
		ldy currentY
		jsr getNodeType
		beq +
			jsr addNodeToQueue
+

	; Sort nodes by score, so we can follow the most promising one
	jsr sortQueueListByScore

	ldy queueCursor
	iny
	cpy queueLength
	beq +
		jmp findPathLoop
+
	
	lda queueLength
	bne +
		; Flooding finished, looks like cannot reach the end point?
		rts
+

	ldy #0
jmp findPathLoop


; Calculates heuristic values (H) for each square using Manhattan distance method
;
; @access private
; @var xEnd - end point x [needs to be set beforehand]
; @var yEnd - end point y [needs to be set beforehand]
; @return void
calculateHeuristics
	ldy #0	
-
	lda pathfinding_xEnd
	sec
	sbc nodesXTable,y
	bcs +
		; Make absolute value
		eor #255
		adc #1
+
	sta pathfinding_temp
	
	lda pathfinding_yEnd
	sec
	sbc nodesYTable,y
	bcs +
		; Make absolute value
		eor #255
		adc #1
+
	adc pathfinding_temp
	sta heuristicTable,y
	iny
	cpy #PATHFINDING_MAX_X*PATHFINDING_MAX_Y-1
	bne -
rts


; @access private
; @var currentNodeId - [needs to be set beforehand]
; @return void
buildTrack
	; Following path from end
	ldx #0
	lda currentNodeId
	jmp pathfinding_trackPointer
-
	tay
	inx
	lda parentNodesTable,y

pathfinding_trackPointer
	sta $1234,x
	cmp startNodeId
	beq +
		jmp -
+

	stx shortestPathLength
	
	; Pull two bytes from stack, so rts will exit from findPath procedure
	pla
	pla
rts


; @access private
; @return void
cleanPathTables
	lda #0
	ldy #PATHFINDING_MAX_X*PATHFINDING_MAX_Y
-
	sta parentNodesTable-1,y
	sta totalScoreTable-1,y
	sta moveCostTable-1,y
	sta nodesListIndexTable-1,y
	sta queueListTable-1,y
	dey
	bne -
rts


.include "constants.asm"
.include "variables.asm"
.include "nodes.asm"
.include "queue.asm"
.include "tables.asm"
