
; These variables can be overwritten
.if PATHFINDING_DEFAULT_VARIABLES_ADDRESSING
	pathfinding_temp = $5
	
	pathfinding_xStart = $6
	currentX = 6 ; this register is going to be reused for current x after
	pathfinding_yStart = $7
	currentY = $7 ; this register is going to be reused for current y after

	pathfinding_xEnd = $8
	pathfinding_yEnd = $9

	queueCursor = $a
	endNodeReached = $b
	
	queueLength = $c
	previousTotalScore = $d

	currentNodeId = $e
	currentNodePositionOnList = $f
	currentMoveCost = $10
	sortOrderAffected = $11
	startNodeId = $12
	endNodeId = $13
	parentNodeId = $14
	shortestPathLength = $15
.fi
