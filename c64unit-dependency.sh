#!/usr/bin/env bash
if [ -d vendor/c64unit ];
then
    cd vendor/c64unit
    git pull https://Commocore@bitbucket.org/Commocore/c64unit.git master
    cd ../../
else
    git clone --origin c64unit git@bitbucket.org:Commocore/c64unit vendor/c64unit
fi
