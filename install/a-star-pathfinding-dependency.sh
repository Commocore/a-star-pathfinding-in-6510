#!/usr/bin/env bash
if [ -d vendor/a-star-pathfinding ];
then
    cd vendor/a-star-pathfinding
    git pull http://Commocore@bitbucket.org/Commocore/a-star-pathfinding-in-6510.git master
    cd ../../
else
    git clone --origin a-star-pathfinding-in-6510 http://Commocore@bitbucket.org/Commocore/a-star-pathfinding-in-6510.git vendor/a-star-pathfinding
fi
