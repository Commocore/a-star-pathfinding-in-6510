@echo off
IF EXIST vendor\a-star-pathfinding (
    cd vendor\a-star-pathfinding
    git pull http://Commocore@bitbucket.org/Commocore/a-star-pathfinding-in-6510.git master
    cd ..\..\
) ELSE (
    git clone --origin a-star-pathfinding-in-6510 http://Commocore@bitbucket.org/Commocore/a-star-pathfinding-in-6510.git vendor/a-star-pathfinding
)
@echo on
