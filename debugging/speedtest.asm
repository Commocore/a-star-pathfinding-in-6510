
	PATHFINDING_DEFAULT_VARIABLES_ADDRESSING = 1 ; if not default, use your own addressing for all variables
	PATHFINDING_DEBUG_MODE = 0 ; additional checkpoints for testing pitfall places
	PATHFINDING_DEBUG_TABLE_POINTER = 0 ; if set, some tables in includes/pathfinding/tables.asm file will be set with PC counter for debug purposes
	PATHFINDING_MANUAL_RUN = 0 ; run program in manual mode, which draws the path, shows messages, needs user's interaction

	PATHFINDING_MAX_X = 13
	PATHFINDING_MAX_Y = 8

	MAX_MAPS = 21 ; number of maps, see includes/maps.asm for all available map setups, or create your own!
	OUTPUT_TRACK_ADDRESS = $2000

	; Variables used for displaying speed test results
	SCREEN_ADDR = $400

	COLOR_RAM = $d800
	MAP_SCREEN_OFFSET = 40*8 + 13

	framesCount = $30 ; counting frames
	rasterlinesLeftLo = $31 ; counting rasterlines left in frame (lo byte)
	rasterlinesLeftHi = $32 ; counting rasterlines left in frame (hi byte)
	
	number1 = $50
	number10 = $51
	number100 = $52
	
	value = $50
	valuePosLo = $51
	valuePosHi = $52
	
	screen = $53 ; and $54
	
	var1 = $55
	var2 = $56

	nextStepCtrl = $57
	nextStepCtrlFireButton = $58
	
	switchPreviousMapCtrl = $59
	switchNextMapCtrl = $5a
	
	aMemDrawSquare = $5b
	xMemDrawSquare = $5c
	yMemDrawSquare = $5d
	
	switchMapProcess = $5e

	mapPointerOffset = $60
	findPathProcess = $61
	map = $62
	word = $63 ; and $64
	
*=$0801
	.word ss,10
	.null $9e,^start
ss	.word 0

start

	lda #0 ; select map (starts from 0)
	sta map

	jsr init
	jsr initMap
	
	ldy #0
	jsr setScreenLocation

	jsr displayResultTableLabels

-
	jsr triggerFindPathProcess
	
-
	; Find path process will run outside of IRQ (but triggered from IRQ at the top of the screen in rasterline = 0), as it tooks more than 1 frame to process
	lda findPathProcess
	cmp #2
	bne +

		inc findPathProcess ; increment by one, so frames will start to be counted in IRQ
		jsr findPath

		lda $d012
		sta rasterlinesLeftLo
		
		lda $d011
		and #%10000000
		sta rasterLinesLeftHi

		lda #0
		sta findPathProcess
		
		jsr displayResultRow

		inc map
		jsr initMap
		
		inc displayResultRow+1
		lda displayResultRow+1
		cmp #MAX_MAPS+2
		beq ++
		
		jmp --

+
	jmp -
+
	; End of speed test
	lda #5
	sta $d020
jmp *


; @access public
; @var map - map id [needs to be set beforehand]
; @return void
initMap
	ldy map
	lda mapAddressLocationLo,y
	sta pathfinding_mapPointer+1
	sta word
	lda mapAddressLocationHi,y
	sta pathfinding_mapPointer+2
	sta word+1

	ldy #PATHFINDING_MAX_X*PATHFINDING_MAX_Y
	lda (word),y
	sta pathfinding_xStart
	
	iny
	lda (word),y
	sta pathfinding_yStart
	
	iny
	lda (word),y
	sta pathfinding_xEnd
	
	iny
	lda (word),y
	sta pathfinding_yEnd
	
	lda #<OUTPUT_TRACK_ADDRESS
	sta pathfinding_trackPointer+1
	lda #>OUTPUT_TRACK_ADDRESS
	sta pathfinding_trackPointer+2
rts


; Include pathfinding algorithm
.include "../src/main.asm"

; Include speed test components
.include "includes/core.asm"
.include "includes/init.asm"
.include "includes/irq-speedtest.asm"
.include "../data/maps.asm"
.include "includes/drawing.asm"
.include "includes/draw-shortest-path.asm"
.include "includes/manual-run-messages.asm"
.include "includes/messages.asm"
.include "includes/debug-tables.asm"

