
; @access public
; @var shortestPathLength [needs to be set beforehand]
; @var queueCursor [needs to be set beforehand]
; @var trackTable [table needs to be filled beforehand]
; @return void
drawShortestPath

	; Set pointers to track
	lda pathfinding_trackPointer+1
	sta drawShortestPathPointer+1
	lda pathfinding_trackPointer+2
	sta drawShortestPathPointer+2

	; We need to go backwards here
	ldy shortestPathLength
	sty queueCursor ; this one can be overwritten now, it's just used as a Zero Page pointer
-
drawShortestPathPointer
	lda $1234,y
	
	; Get x and y position by node id
	tay
	lda nodesXTable,y
	tax
	lda nodesYTable,y
	tay
	lda #5
	jsr drawSquare
		
	dec queueCursor
	ldy queueCursor
	bne -
rts
