.enc screen

currentMapText .text "Map: "

currentNodeText .text "Node: $@ (@:@)   "

cannotFindExitText .text "Cannot find exit. There's no hope."

finalText .text "Finished."

finalText2 .text "Shortest path found. Length: @."

speedTestResultLabelsText .text "Map Frames Rasterlines Exit Length"

endNodeReachedText .text "YN-"

rasterlinesLeftHiText .text "255+"

toHexTable .text "0123456789abcdef"

.enc none
