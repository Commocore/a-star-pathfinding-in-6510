
; Wait for joystick port 2 fire button or RETURN key pressed, then wait for release
;
; @access public
; @return void
waitingStep

	; Looking for pressing RETURN key
	lda nextStepCtrl
	bne +
		lda #%11111110
		sta $dc00
		lda $dc01
		and #%00000010
		bne +
			inc nextStepCtrl
			rts
+

	; Looking for hit of fire button
	lda nextStepCtrlFireButton
	bne +
		lda #%00010000
		bit $dc00
		bne +
			inc nextStepCtrlFireButton
			rts
+


	lda switchMapProcess
	beq +
		rts
+

jmp waitingStep


; @access public
; @return void
displayCurrentStatusMessage
	jsr cleanMessages
	ldy #0
	jsr setScreenLocation
	ldy #0
-
	lda currentMapText,y
	sta (screen),y
	iny
	cpy #size(currentMapText)
	bne -
	
	lda map
	jsr extract8bitValueToDec
	jsr displayNumber
rts


; @access public
; @return void
displayCurrentNodeMessage
	sty var2

	ldy #1
	jsr setScreenLocation
	
	ldy #0
	ldx #0
	stx var1
-
	lda currentNodeText,x
	bne ++++
		inc var1
		lda var1
		cmp #1
		bne +
			lda var2 ; get node id
			jsr extract8bitValueToHex
			jsr displayHex
			jmp +++++
+
		cmp #2
		bne +
			lda currentX ; get currentX
			jmp ++
+
		cmp #3
		bne +++
		lda currentY ; get currentY
+
		jsr extract8bitValueToDec
		jsr displayNumber
		jmp ++
+
	sta (screen),y
	iny
+
	inx
	cpx #size(currentNodeText)
	bne -
	
	lda #3
	ldx currentX
	ldy currentY
	jsr drawSquare
rts


; @access public
; @return void
displayCannotFindExitMessage
	jsr cleanMessages

	ldy #0
	jsr setScreenLocation
	ldy #0
-
	lda cannotFindExitText,y
	sta (screen),y
	iny
	cpy #size(cannotFindExitText)
	bne -
	
	jsr waitingStep
	jsr cleanMessages
	.if PATHFINDING_MANUAL_RUN
		jsr drawMap
	.fi
rts


; @access public
; @return void
displayFinalMessage
	jsr cleanMessages

	; First line of final text
	ldy #0
	jsr setScreenLocation
	ldx #0
-
	lda finalText,x
	sta (screen),y
	iny
	inx
	cpx #size(finalText)
	bne -
	
	; Second line of final text
	ldy #1
	jsr setScreenLocation
	ldy #0
	ldx #0
-
	lda finalText2,x
	bne +
		lda shortestPathLength
		jsr extract8bitValueToDec
		jsr displayNumber
		jmp ++
+
	sta (screen),y
	iny
+
	inx
	cpx #size(finalText2)
	bne -
rts


; @access public
; @return void
cleanMessages
	ldx #39
	lda #32
-
	sta SCREEN_ADDR,x
	sta SCREEN_ADDR + 40,x
	sta SCREEN_ADDR + 80,x
	sta SCREEN_ADDR + 120,x
	dex
	bpl -
rts


; @access public
; @return void
displayResultTableLabels
	ldy #0
-
	lda speedTestResultLabelsText,y
	sta (screen),y
	iny
	cpy #size(speedTestResultLabelsText)
	bne -
rts


; @access public
; @return void
setWhiteForeground
	; Set Color RAM colour
	ldy #0
	lda #1
-
	.for i=0, i<=3, i=i+1
		sta COLOR_RAM+250*i,y
	.next
	iny
	cpy #250
	bne -
rts


.if PATHFINDING_MANUAL_RUN = 0

; @access public
; @return void
displayResultRow
	ldy #1
	jsr setScreenLocation

	ldy #0
	lda map
	jsr extract8bitValueToDec
	jsr displayNumber
	
	; Display frames count
	lda framesCount
	jsr extract8bitValueToDec
	ldy #4
	jsr displayNumber
	
	; Display rasterlines left
	ldy #11
	lda rasterlinesLeftHi
	beq +
		lda rasterlinesLeftHiText
		sta (screen),y
		
		lda rasterlinesLeftHiText+1
		iny
		sta (screen),y

		lda rasterlinesLeftHiText+2
		iny
		sta (screen),y
		
		lda rasterlinesLeftHiText+3
		iny
		sta (screen),y
		
		iny
+

	lda rasterlinesLeftLo
	jsr extract8bitValueToDec
	jsr displayNumber
	
	; Display end node reached or not, and shortest path length if any
	lda endNodeReached
	beq +
		ldy #23
		lda endNodeReachedText
		sta (screen),y
		
		lda shortestPathLength
		jsr extract8bitValueToDec
		ldy #28
		jsr displayNumber
	jmp ++
+
	ldy #23
	lda endNodeReachedText+1
	sta (screen),y
	
	ldy #28
	lda endNodeReachedText+2
	sta (screen),y
+
rts

.fi
