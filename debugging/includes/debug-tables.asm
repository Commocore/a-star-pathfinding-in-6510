verticalColorRamTableLo
	.for y=0, y<PATHFINDING_MAX_Y, y=y+1
		.byte <$d800+y*40 + MAP_SCREEN_OFFSET
	.next

verticalColorRamTableHi
	.for y=0, y<PATHFINDING_MAX_Y, y=y+1
		.byte >$d800+y*40 + MAP_SCREEN_OFFSET
	.next

verticalScreenTableLo
	.for y=0, y<PATHFINDING_MAX_Y, y=y+1
		.byte <$400+y*40 + MAP_SCREEN_OFFSET
	.next

verticalScreenTableHi
	.for y=0, y<PATHFINDING_MAX_Y, y=y+1
		.byte >$400+y*40 + MAP_SCREEN_OFFSET
	.next

screenLocationLo
	.for i=0, i<=24, i=i+1
		.byte <SCREEN_ADDR+40*i
	.next

screenLocationHi
	.for i=0, i<=24, i=i+1
		.byte >SCREEN_ADDR+40*i
	.next

xStartMem
	.byte 0
	
yStartMem
	.byte 0
	
xEndMem
	.byte 0
	
yEndMem
	.byte 0
