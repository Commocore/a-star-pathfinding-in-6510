
; @access public
; @var number100 [needs to be set beforehand]
; @var number10 [needs to be set beforehand]
; @var number1 [needs to be set beforehand]
; @return void
displayNumber
	clc
	lda number100
	beq +
		; Display all 3 digits (hundreds)
		adc #48
		sta (screen),y
		iny
		
		lda number10
		adc #48
		sta (screen),y
		iny
		
		lda number1
		adc #48
		sta (screen),y
		iny

		rts
+

	lda number10
	beq +
		; Display 2 digits (tens)
		adc #48
		sta (screen),y
		iny
+

	lda number1
	adc #48
	sta (screen),y
	iny
rts


; @access public
; @var valuePosHi [needs to be set beforehand]
; @var valuePosLo [needs to be set beforehand]
; @return void
displayHex
	txa
	pha

	clc
	ldx valuePosHi
	lda toHexTable,x
	sta (screen),y
	iny

	ldx valuePosLo
	lda toHexTable,x
	sta (screen),y
	iny
	
	pla
	tax
rts


; @access public
; @param A
; @sets number_1
; @sets number_10
; @sets number_100
; @return void
extract8bitValueToDec

	pha

	lda #0
	sta number100
	sta number10
	sta number1

	pla

	; hundreds
	sec
-
		cmp #100
		bcc +
		sbc #100
		inc number100
		jmp -
+

	; tens
	sec
-
		cmp #10
		bcc +
		sbc #10
		inc number10
		jmp -
+

	; ones
	clc
	sta number1

rts


; @access public
; @param A
; @sets valuePosHi
; @sets valuePosLo
; @return void
extract8bitValueToHex
	sta value

	; n * 16^2
	lsr
	lsr
	lsr
	lsr
	sta valuePosHi

	; n * 16^1
	lda valuePosHi
	asl
	asl
	asl
	asl
	sta valuePosLo
	
	lda value
	sec
	sbc valuePosLo
	sta valuePosLo
rts


; @access public
; @param Y - screen row
; @return void
setScreenLocation
	lda screenLocationLo,y
	sta screen
	lda screenLocationHi,y
	sta screen+1
rts
