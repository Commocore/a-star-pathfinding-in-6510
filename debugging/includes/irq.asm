
; @access public
; @return void
irq
	pha
	txa
	pha
	tya
	pha

	inc $d019

	; hit fire button to start, or press RETURN key
	jsr waitingForAction

	.if PATHFINDING_MANUAL_RUN
		jsr waitingStepKeys
	.fi
	
	pla
	tay
	pla
	tax
	pla
rti


; @access public
; @var findPathProcess [needs to be set beforehand]
; @return void
triggerFindPathProcess
	lda findPathProcess
	bne +
		lda #1
		sta findPathProcess
+
rts


; Wait for joystick port 2 fire button pressed, or RETURN key
;
; @access public
; @return void
waitingForAction
	; Looking for RETURN key pressed
	lda #%11111110
	sta $dc00
	lda $dc01
	and #%00000010
	bne +
		jsr triggerFindPathProcess
+

	; Looking for fire button hit
	lda #%00010000
	bit $dc00
	bne +
		jsr triggerFindPathProcess
+

.if PATHFINDING_MANUAL_RUN

	; Looking for pressing F1 to switch to the previous map
	lda switchPreviousMapCtrl
	bne +
		lda #%11111110
		sta $dc00
		lda $dc01
		and #%00010000
		bne +
			inc switchPreviousMapCtrl
			
			lda map
			beq +
				jsr haltFindPathExecution
				dec map
+

	; Looking for pressing F3 to switch to the next map
	lda switchNextMapCtrl
	bne +
		lda #%11111110
		sta $dc00
		lda $dc01
		and #%00100000
		bne +
			inc switchNextMapCtrl
			
			lda map
			cmp #MAX_MAPS
			beq +
				jsr haltFindPathExecution
				inc map
+

.fi

rts


.if PATHFINDING_MANUAL_RUN

; @access public
; @return void
waitingStepKeys

	; RETURN key release check
	lda #%11111110
	sta $dc00
	lda $dc01
	and #%00000010
	beq +
		; If RETURN key is not pressed, reset next step ctrl
		lda #0
		sta nextStepCtrl
+

	; Fire button (joystick port 2) release check
	lda #%00010000
	bit $dc00
	beq +
		; If fire button is not pressed, reset next step ctrl
		lda #0
		sta nextStepCtrlFireButton
+

	; F1 key release check
	lda #%11111110
	sta $dc00
	lda $dc01
	and #%00010000
	beq +
		; If F1 key is not pressed, reset switch previous map ctrl
		lda #0
		sta switchPreviousMapCtrl
+

	; F3 key release check
	lda #%11111110
	sta $dc00
	lda $dc01
	and #%00100000
	beq +
		; If F3 key is not pressed, reset switch next map ctrl
		lda #0
		sta switchNextMapCtrl
+

rts


; @access public
; @return void
haltFindPathExecution
	lda #1
	sta switchMapProcess
rts

.fi
