
; @access public
; @var map - map id [needs to be set beforehand]
; @return void
drawMap
	ldy map
	lda mapAddressLocationLo,y
	sta drawMapPointer+1
	lda mapAddressLocationHi,y
	sta drawMapPointer+2

	lda #174
	.for y=0, y<PATHFINDING_MAX_Y, y=y+1
		ldx #PATHFINDING_MAX_X-1
-
		sta SCREEN_ADDR + y*40 + MAP_SCREEN_OFFSET,x
		dex
		bpl -
	.next

	ldx #0
	ldy #0
	sty mapPointerOffset
-
	sty var1
	ldy mapPointerOffset
drawMapPointer
	lda $1234,y
	ldy var1
	jsr drawSquare
	ldy var1
	
	inc mapPointerOffset
	inx
	cpx #PATHFINDING_MAX_X
	bne +
		ldx #0
		iny
		cpy #PATHFINDING_MAX_Y
		bne +
			rts
+
	jmp -


; @access public
; @param A - colour number
; @param X - position x
; @param Y - position y
; @return void
drawSquare
	sta aMemDrawSquare
	stx xMemDrawSquare
	sty yMemDrawSquare

	clc
	lda verticalColorRamTableLo,y
	adc xMemDrawSquare
	sta setColorRamSquare+1
	lda verticalColorRamTableHi,y
	adc #0
	sta setColorRamSquare+2

	cpx xStartMem
	bne +
		cpy yStartMem
		bne +
			lda #13
			jmp setColorRamSquare
+

	cpx xEndMem
	bne +
		cpy yEndMem
		bne +
			lda #2
			jmp setColorRamSquare
+
	
	ldy aMemDrawSquare
	lda squareColorTable,y
setColorRamSquare
	sta $1234
	
	lda aMemDrawSquare
	ldx xMemDrawSquare
	ldy yMemDrawSquare
rts


squareColorTable
	.byte 11 ; non accessible
	.byte 12 ; track
	.byte 5 ; node on opened list
	.byte 1 ; current node
	.byte 4 ; closed node
	.byte 8 ; shortest path
