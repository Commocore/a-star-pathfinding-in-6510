
; @access public
; @return void
irq
	pha
	txa
	pha
	tya
	pha

	inc $d019

	lda findPathProcess
	beq ++
		cmp #1
		bne +
			inc findPathProcess ; set to 2 so findPath will be triggered exactly on the top of frame
			jmp ++
+
		inc framesCount
+

	pla
	tay
	pla
	tax
	pla
rti


; @access public
; @var findPathProcess [needs to be set beforehand]
; @return void
triggerFindPathProcess
	lda findPathProcess
	bne +

		lda #0
		sta framesCount

		lda #1
		sta findPathProcess
+
rts

