
	PATHFINDING_DEFAULT_VARIABLES_ADDRESSING = 1 ; if not default, use your own addressing for all variables
	PATHFINDING_DEBUG_MODE = 1 ; additional checkpoints for testing pitfall places
	PATHFINDING_DEBUG_TABLE_POINTER = 0 ; if set, some tables in includes/pathfinding/tables.asm file will be set with PC counter for debug purposes
	PATHFINDING_MANUAL_RUN = 1 ; run program in manual mode, which draws the path, shows messages, needs user's interaction
	
	PATHFINDING_MAX_X = 13
	PATHFINDING_MAX_Y = 8

	MAX_MAPS = 21 ; number of maps, see includes/maps.asm for all available map setups, or create your own!
	OUTPUT_TRACK_ADDRESS = $2000

	.if PATHFINDING_MANUAL_RUN
		; Variables used for displaying messages and drawing demo on screen
		SCREEN_ADDR = $400

		COLOR_RAM = $d800
		MAP_SCREEN_OFFSET = 40*8 + 13

		number1 = $50
		number10 = $51
		number100 = $52
		
		value = $50
		valuePosLo = $51
		valuePosHi = $52
		
		screen = $53 ; and $54
		
		var1 = $55
		var2 = $56

		nextStepCtrl = $57
		nextStepCtrlFireButton = $58
		
		switchPreviousMapCtrl = $59
		switchNextMapCtrl = $5a
		
		aMemDrawSquare = $5b
		xMemDrawSquare = $5c
		yMemDrawSquare = $5d
		
		switchMapProcess = $5e
	.fi

	mapPointerOffset = $60
	findPathProcess = $61
	map = $62
	word = $63

*=$0801
	.word ss,10
	.null $9e,^start
ss	.word 0

start

	lda #10 ; select map (starts from 0), can be changed with F1/F3 keys
	sta map

	jsr init
	jsr initMap
	jsr drawMap
	
	.if PATHFINDING_MANUAL_RUN
		jsr displayCurrentStatusMessage
	.fi
	
	jsr triggerFindPathProcess
	
-
	; Find path process will run outside of IRQ (but triggered from IRQ at the top of the screen in rasterline = 0), as it tooks more than 1 frame to process
	lda findPathProcess
	beq +++++

		jsr findPath
		
		.if PATHFINDING_MANUAL_RUN
		
			; If switch map triggered, prepare everything once again but for new map
			jsr isSwitchMapTriggered
			beq +
				jsr switchMapPrepare
				jmp -
+
		
			lda endNodeReached
			beq ++
				; Path found
				jsr drawShortestPath
				jsr displayFinalMessage
				jsr waitingStep
				jsr isSwitchMapTriggered
				beq +
					jsr switchMapPrepare
					jmp -
+
				jmp ++
+
				; Path to the exit not found at all...
				jsr displayCannotFindExitMessage
				jsr isSwitchMapTriggered
				beq +
					jsr switchMapPrepare
					jmp -
+
			jsr cleanMessages
			jsr initMap
			jsr drawMap
		
		.fi
		
		lda #0
		sta findPathProcess
+
	jmp -


; @access public
; @var map - map id [needs to be set beforehand]
; @return void
initMap
	ldy map
	lda mapAddressLocationLo,y
	sta pathfinding_mapPointer+1
	sta word
	lda mapAddressLocationHi,y
	sta pathfinding_mapPointer+2
	sta word+1

	ldy #PATHFINDING_MAX_X * PATHFINDING_MAX_Y
	lda (word),y
	sta pathfinding_xStart
	sta xStartMem
	
	iny
	lda (word),y
	sta pathfinding_yStart
	sta yStartMem
	
	iny
	lda (word),y
	sta pathfinding_xEnd
	sta xEndMem
	
	iny
	lda (word),y
	sta pathfinding_yEnd
	sta yEndMem
	
	lda #<OUTPUT_TRACK_ADDRESS
	sta pathfinding_trackPointer+1
	lda #>OUTPUT_TRACK_ADDRESS
	sta pathfinding_trackPointer+2
rts


.if PATHFINDING_MANUAL_RUN

; @access private
; @return A
isSwitchMapTriggered
	lda switchMapProcess
	beq +
		lda #1 ; true
		rts
+
	lda #0 ; false
rts


; @access public
; @return void
switchMapPrepare
	jsr initMap
	jsr drawMap
	jsr displayCurrentStatusMessage
	jsr triggerFindPathProcess
	lda #0
	sta switchMapProcess
rts

.fi


; Include pathfinding algorithm
.include "../src/main.asm"

; Include demo components
.include "includes/init.asm"
.include "includes/irq.asm"
.if PATHFINDING_MANUAL_RUN
	.include "includes/core.asm"
	.include "includes/drawing.asm"
	.include "includes/draw-shortest-path.asm"
	.include "includes/debug-tables.asm"
	.include "includes/manual-run-messages.asm"
	.include "includes/messages.asm"
.fi
.include "../data/maps.asm"

